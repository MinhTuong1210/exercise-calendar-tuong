
import React from 'react';
import Container from './components/Container';
import './App.css';



const dateOfMonth = (data) => {
  let d = new Date()
  let day = d.getDay()
  let date = d.getDate()
  for (let da of data) {
    if (da.day === day) {
      da.dayMonth = date
      da.dayCurrent = true
    }
    else {
      da.dayMonth = date + da.day - day
    }
  }

  return data
}

function App() {

  const dataDate = [{ dayWeek: "MON", day: 1, dayMonth: '', dayCurrent: false },
  { dayWeek: "TUE", day: 2, dayMonth: '', dayCurrent: false },
  { dayWeek: "WED", day: 3, dayMonth: '', dayCurrent: false },
  { dayWeek: "THU", day: 4, dayMonth: '', dayCurrent: false },
  { dayWeek: "FRI", day: 5, dayMonth: '', dayCurrent: false },
  { dayWeek: "SAT", day: 6, dayMonth: '', dayCurrent: false },
  { dayWeek: "SUN", day: 7, dayMonth: '', dayCurrent: false }]


  return (
    <div className="App">
      <header className="App-header">
        <div style={{ display: 'flex' }}>
          {
            dateOfMonth(dataDate).map((value, key) => {
              return (<div key={key} style={{ margin: "0px 10px", width: "100vw" }}>
                <Container date={value.dayWeek} dateOfMonth={value.dayMonth} dayCurrent={value.dayCurrent} />
              </div>)
            })
          }
        </div>

       


      </header>
    </div>
  );
}

export default App;
