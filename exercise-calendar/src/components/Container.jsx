import React, { useState } from 'react';
import { Container, Draggable } from "react-smooth-dnd";

import Table from './Table';

const initList = ["item 1"];

const ContainerDate = (props) => {

    const { date, dateOfMonth, dayCurrent } = props

    const [list, setList] = useState(initList)

    const sort = (
        _tempList,
        { removedIndex, addedIndex, payload }
    ) => {

        if (removedIndex === null && addedIndex === null) return _tempList;

        const tempList = [..._tempList];

        const itemToAdd =
            removedIndex !== null ? tempList.splice(removedIndex, 1)[0] : payload;

        if (addedIndex !== null) {
            tempList.splice(addedIndex, 0, itemToAdd);
        }

        return tempList;
    };


    return (
        <div>
            <p style={{ fontWeight: "500" }}>{date}</p>
            <div style={{ height: "calc(100vh + auto)", borderRadius: "10px", color: "black", fontSize: "16px", backgroundColor: "#F6F7FA", padding: "10px" }} >
                {dayCurrent ? <b style={{ color: "#5A57CB" }}>{dateOfMonth}</b> : <b>{dateOfMonth}</b>}
                <Container
                    groupName='test'
                    getChildPayload={(i) => list[i]}
                    onDrop={(e) => setList(sort(list, e))}
                >
                    {
                        list.map((value, key) => {
                            return <Draggable key={key} ><Table /></Draggable>
                        })
                    }
                </Container>


            </div>
        </div>

    );
}

export default ContainerDate;