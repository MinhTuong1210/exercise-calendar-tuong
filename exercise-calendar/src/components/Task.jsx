import React, {useState} from 'react';


const Task = (props) => {
    const { items } = props

    const [valueText, setValueText] = useState(items.text)
    const [valueNumberExercise, setValueNumberExercise] = useState(items.numberExercise)
    const [valueLb, setValueLb] = useState(items.lb)

    const handlleChange = (e) => {
        setValueText(e.target.value)
    }


    return (
        <div style={{ padding: "5px", border: "1px solid #DEDEE0", borderRadius: "8px", boxShadow: "#DEDEE0", display: "flex", flexDirection: "column", marginBottom: "20px" }}>
            <input style={{
                alignSelf: "end",
                border: "none",
                fontWeight: "bold",
                width: "80px",
                backgroundColor: "#F6F7FA",
                outlineColor: "#393B81",
                whiteSpace: "nowrap",
                overflow: "hidden",
                textOverflow: "ellipsis"
            }} value={valueText} onchange={handlleChange}/>

            <div style={{ display: "flex", justifyContent: "space-between" }}>
                <input style={{ border: "none",
                width: "30px",
                backgroundColor: "#F6F7FA",
                outlineColor: "#393B81",
                whiteSpace: "nowrap",
                overflow: "hidden",
                textOverflow: "ellipsis"}}  value={valueNumberExercise} onchange={e => setValueNumberExercise(e.target.value)} />

                <input style={{ border: "none",
                width: "70px",
                backgroundColor: "#F6F7FA",
                outlineColor: "#393B81",
                whiteSpace: "nowrap",
                overflow: "hidden",
                textOverflow: "ellipsis"}}  value={valueLb} onchange={e => setValueLb(e.target.value)}/>
            </div>

        </div>
    );
}

export default Task;