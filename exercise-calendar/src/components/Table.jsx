import React, { useState } from 'react';
import { Container, Draggable } from "react-smooth-dnd";


import '../App.css'
import Add from "../assest/add.png"
import Task from "../components/Task"



const Table = () => {

    const [value, setValue] = useState();
    const handleName = (e) => {
        setValue(e.target.value)
    }

    const initListExercise = [
        { id: 1, text: "Exericse A", order: 0, numberExercise: "1x", lb: "30 lb x 3" },
        { id: 2, text: "Exercise B", order: 1, numberExercise: "3x", lb: "40 lb x 4" },
    ]

    const [listExercise, setListExercise] = useState(initListExercise)

    const sort = (
        _tempList,
        { removedIndex, addedIndex, payload }
    ) => {
        if (removedIndex === null && addedIndex === null) return _tempList;
        const tempList = [..._tempList];
        const itemToAdd =
            removedIndex !== null ? tempList.splice(removedIndex, 1)[0] : payload;
        if (addedIndex !== null) {
            tempList.splice(addedIndex, 0, itemToAdd);
        }

        return tempList;
    };

    const addExercise = (e) => {
        initListExercise.push(
            {
                id: initListExercise.length+1, text: "Exercise " + String.fromCharCode(65+initListExercise.length) , numberExercise: "2x", lb: "3b lb x 5"
            }
        )
    }

    return (
        <div style={{ padding: "10px", border: "2px solid #DEDEE0", borderRadius: "10px", boxShadow: "#DEDEE0", display: "flex", flexDirection: "column" }}>
            <div style={{ display: "flex", width: "auto" }}>
                <input style={{
                    border: "none",
                    color: "#5A57CB",
                    fontWeight: "bold",
                    width: "80%",
                    backgroundColor: "#F6F7FA",
                    outlineColor: "#393B81",
                    whiteSpace: "nowrap",
                    overflow: "hidden",
                    textOverflow: "ellipsis"
                }} value={value} placeholder="NAME EXERCISE" />
                <button style={{ color: "#C4C4C4", border: "none", backgroundColor: "#F6F7FA", cursor: "pointer" }} onClick={handleName} ><i class="material-icons">more_horiz</i></button>
            </div>
            <Container groupName='test'
                getChildPayload={(i) => listExercise[i]}
                onDrop={(e) => setListExercise(sort(listExercise, e))}>
                {listExercise.map((value, key) => {
                    return (<Draggable key={key} ><Task items={value} /></Draggable>)
                })}
            </Container>
            <button style={{ border: "none", backgroundColor: "#F6F7FA", alignSelf: "end", cursor: "pointer", objectFit: "scale-down" }} onClick={addExercise()} ><img src={Add} width={"20px"} height={"20px"} alt="add exercise" /></button>
        </div>


    );
};



export default Table;